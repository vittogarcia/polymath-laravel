<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyTestMail;
use Validator;
use Alert;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all(), time() . '_' . $request->name . '_' . $request->logo->extension());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'website' => 'required',
            'logo' => 'required|mimes:jpg,png,jpeg|max:1000'
        ]);

        if($validator->fails()) {
            return redirect('companies')->withInput()->with('toast_error', 'Error to create...');
        }
        else {
            try {
                // rename image
                $newImage = time() . '_' . $request->name . '.' . $request->logo->extension();
                $company = new Company();
                $company->name = $request->input('name');
                $company->email = $request->input('email');
                $company->website = $request->input('website');
                $company->logo = $newImage;
                $company->save();
                // Move to Path
                $request->logo->move(public_path('upload_images'), $newImage);

                // Send Mailgun
                Mail::to('vmoreno85@hotmail.com')->send(new MyTestMail());

                return redirect('companies')->with('toast_success', 'Company created succefully...');

            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withInput()->with('toast_error', 'Error to create new Company. Please check your info...');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Company $company)
    {
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'website' => 'required'
        ]);

        if($validator->fails()){
            return redirect('companies')->withInput()->with('toast_error', 'Error to upate...');
        }
        else {
            if($request->hasFile('logo')) {
                $newImage = time() . '_' . $request->name . '.' . $request->logo->extension();
                $request->logo->move(public_path('upload_images'), $newImage);
            }
            else {
                $newImage = $company->logo;
            }
            try {
                Company::whereId($company->id)->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'website' => $request->input('website'),
                    'logo' => $newImage
                ]);

                return redirect('companies')->with('toast_success', 'Company updated Succesfully...');

            } catch(\Illuminate\Database\QueryException $ex) {
                return back()->withInput()->with('toast_error', 'Error to update info...');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Company $company)
    {
        try {
            if(File::exists(public_path('upload_images/'.$company->logo))) {
                File::delete(public_path('upload_images/'.$company->logo));
            }
            $company = Company::find($company->id)->delete();
            return redirect('companies')->with('toast_success', 'Company deleted succefully...');
        } catch(\Illuminate\Database\QueryException $ex) {
            return back()->withInput()->with('toast_error', 'Error to delete company. Check info...');
        }
    }
}
