<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Company;
use Illuminate\Http\Request;
use Validator;
use Alert;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate(10);
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('employees.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'company' => 'required'
        ]);

        if($validator->fails()) {
            return redirect('employees')->withInput()->with('toast_error', $validator->errors());
        }
        else {
            try {
                $employee = new Employee();
                $employee->first_name = $request->input('first_name');
                $employee->last_name = $request->input('last_name');
                $employee->email = $request->input('email');
                $employee->phone = $request->input('phone');
                $employee->company_id = $request->input('company');
                $employee->save();

                return redirect('employees')->with('toast_success', 'Employee created succefully...');

            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withInput()->with('toast_error', 'Error to create new Employee. Please check your info...');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $companies = Company::all();
        return view('employees.edit', compact('companies', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'company' => 'required'
        ]);

        if($validator->fails()) {
            return redirect('employees')->withInput()->with('toast_error', $validator->errors());
        }
        else{
            try{
                Employee::whereId($employee->id)->update([
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'company_id' => $request->input('company')
                ]);

                return redirect('employees')->with('toast_success', 'Employee updated Succesfully...');                
            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withInput()->with('toast_error', 'Error to upate Employee. Please check your info...');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Employee $employee)
    {
        try {
            $employee = Employee::find($employee->id)->delete();
            return redirect('employees')->with('toast_success', 'Employee deleted succefully...');
        } catch(\Illuminate\Database\QueryException $ex) {
            return back()->withInput()->with('toast_error', 'Error to delete employee. Check info...');
        }
    }
}
