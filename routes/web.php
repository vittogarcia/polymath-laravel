<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyTestMail;

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/////////////////////// COMPANY ROUTES ////////////////////////
Route::resource('companies', CompanyController::class);

/////////////////////// EMPLOYEES ROUTES ////////////////////////
Route::resource('employees', EmployeeController::class);

////////////////////// LANGUAGE /////////////////////////
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

/////////////////////// MAILGUN ///////////////////////
Route::get('/send', function(){
    Mail::to('vmoreno85@hotmail.com')->send(new MyTestMail());
    return redirect('home')->with('toast_success', 'Email send succesfully...');
});