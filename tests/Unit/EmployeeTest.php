<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Employee;
use App\Models\Company;
//use PHPUnit\Framework\TestCase;
use Illuminate\Database\Eloquent\Collection;

class EmployeeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_a_employee_has_many_companies()
    {   
        $employees = new Employee();
        $this->assertInstanceOf(Collection::class, $employees->companiestest);
        //$this->assertTrue(true);
    }
}
