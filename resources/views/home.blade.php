@extends('layouts.app')

@section('page_title')
Welcome {{ Auth::user()->name }}
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-body">
        <div class="container-fluid text-center">
            <img src="{{ asset('img/logofull.png') }}" width="50%" alt="">

        </div>
    </div>
</div>
@endsection