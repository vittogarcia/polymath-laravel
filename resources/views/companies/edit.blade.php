@extends('layouts.app')

@section('page_title')
Companies
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-body">
    	<h5>Edit Company</h5>
        <form action="/companies/{{ $company->id }}" method="POST" enctype="multipart/form-data">@csrf @method('PUT')
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" placeholder="name of company" value="{{ $company->name }}" required>
                </div>
                <div class="col-sm-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="mail@mail.com" value="{{ $company->email }}" required>
                </div>              
                <div class="col-sm-3">
                    <label for="website">Website</label>    
                    <input type="text" name="website" class="form-control" placeholder="wwww.domain.com" value="{{ $company->website }}" required>
                </div>
                <div class="col-sm-3">
                    <label for="logo">Logo</label>
                    <input type="file" class="form-control-file" name="logo" accept="image/*" data-max-size="1000">
                    <small class="w-100">Only Image 100x100</small>
                </div>   
            </div>        
            <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </form>                             
    </div>
</div>
@endsection