@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('page_title')
Companies
@endsection

@section('content')
<div class="card card-primary card-outline">
	<div class="card-body">
		<div class="d-flex justify-content-end">
			<a class="btn btn-app" href="{{ url('companies/create') }}">
              <i class="fas fa-plus"></i> New Company
            </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-hover table-sm" id="companies_table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Website</th>
					<th>Logo</th>
					<th>Show</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($companies as $c)
				<tr>
					<td>{{ $c->name }}</td>
					<td>{{ $c->email }}</td>
					<td>
						<a href="http://{{ $c->website }}" class="btn-link" target="_blank">{{ $c->website }}</a>
					</td>
					<td>
						<a href="{{ asset('upload_images/' . $c->logo) }}" data-toggle="lightbox" data-title="image_{{ $c->logo }}" data-gallery="gallery">
							<img src="{{ asset('upload_images/'. $c->logo) }}" alt="{{ $c->logo }}" class="img-fluid mb-2" width="100px">
						</a>
					</td>
					<td>
						<a href="/companies/{{ $c->id }}" class="btn"><i class="far fa-eye"></i></a>
					</td>
					<td>
						<a href="/companies/{{ $c->id }}/edit" class="btn"><i class="far fa-edit"></i></a>
					</td>
					<td>
						<form action="/companies/{{ $c->id }}" method="POST">@csrf @method('DELETE')
							<button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
</div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>
<script>
    $(document).ready( function () {
        $('#companies_table').DataTable({
            "paging": true,
            "ordering": true,
            "pageLength": 10,
            "lengthMenu": [10, 20, 30, 40, 50]
        });
    });       
</script>
@endsection