@extends('layouts.app')

@section('page_title')
Companies
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-body">
    	<h5>Register New Company</h5>
        <form action="/companies" method="POST" enctype="multipart/form-data">@csrf
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" placeholder="name of company" required>
                </div>
                <div class="col-sm-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="mail@mail.com" required>
                </div>              
                <div class="col-sm-3">
                    <label for="website">Website</label>    
                    <input type="text" name="website" class="form-control" placeholder="wwww.domain.com" required>
                </div>
                <div class="col-sm-3">
                    <label for="logo">Logo</label>
                    <input type="file" class="form-control-file" name="logo" accept="image/*" data-max-size="1000" required>
                    <small class="w-100">Only Image 100x100</small>
                </div>   
            </div>        
            <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </form>                             
    </div>
</div>
@endsection