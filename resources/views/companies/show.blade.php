@extends('layouts.app')

@section('page_title')
Details
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="card">
                <img class="card-img-top" src="{{ asset('upload_images/'.$company->logo) }}" alt="Company Logo">
                <div class="card-body">
                    <h5 class="text-center"><strong>Name: </strong>{{ $company->name }}</h5>
                    <h5 class="text-center"><strong>Email: </strong>{{ $company->email }}</h5>
                    <h5 class="text-center"><strong>Website: </strong>{{ $company->website }}</h5>
                </div>
                <div class="card-footer text-muted text-center">
                    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
              </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>                                                                          
@endsection