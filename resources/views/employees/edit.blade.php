@extends('layouts.app')

@section('page_title')
Employees
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-body">
    	<h5>Edit Employee</h5>
        <form action="/employees/{{ $employee->id }}" method="POST">@csrf @method('PUT')
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" placeholder="first name" value="{{ $employee->first_name }}" required>
                </div>
                <div class="col-sm-4">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" placeholder="last name" value="{{ $employee->last_name }}" required>
                </div>
                <div class="col-sm-4">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="mail@mail.com" value="{{ $employee->email }}" required>
                </div>
            </div>
            <div class="form-group row">              
                <div class="col-sm-6">
                    <label for="phone">Phone Number</label>    
                    <input type="number" name="phone" class="form-control" placeholder="phone number" value="{{ $employee->phone }}" maxlength="10" required>
                </div>
                <div class="col-sm-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control" required>
                        <option value="{{ $employee->company_id }}">{{ $employee->companies->name }}</option>
                        @foreach($companies as $c)
                        <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>   
            </div>        
            <div class="form-group">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </form>                             
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#company').select2({theme:'bootstrap4'});
    });
</script>
@endsection