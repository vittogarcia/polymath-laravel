@extends('layouts.app')

@section('page_title')
Details
@endsection

@section('content')
<div class="container">
<div class="card text-center">
  <div class="card-header">
    <h5>Employee</h5>
  </div>
  <div class="card-body">
    <h6 class="text-center"><strong>Full Name: </strong>{{ $employee->first_name }} {{ $employee->last_name }}</h6>
    <h6 class="text-center"><strong>EMail: </strong>{{ $employee->email }}</h6>
    <h6 class="text-center"><strong>Phone Number: </strong>{{ $employee->phone }}</h6>
    <h6 class="text-center"><strong>Company: </strong>{{ $employee->companies->name }}</h6>
  </div>
  <div class="card-footer text-muted">
    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
  </div>
</div>
</div>                                                                          
@endsection