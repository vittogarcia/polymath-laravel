@extends('layouts.app')

@section('page_title')
Employees
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-body">
    	<h5>Register New Employee</h5>
        <form action="/employees" method="POST">@csrf
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" placeholder="first name" required>
                </div>
                <div class="col-sm-4">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" placeholder="last name" required>
                </div>
                <div class="col-sm-4">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="mail@mail.com" required>
                </div>
            </div>
            <div class="form-group row">              
                <div class="col-sm-6">
                    <label for="phone">Phone Number</label>    
                    <input type="number" name="phone" class="form-control" placeholder="phone number" maxlength="10" required>
                </div>
                <div class="col-sm-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control" required>
                        <option value="">Select Company</option>
                        @foreach($companies as $c)
                        <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>   
            </div>        
            <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </form>                             
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#company').select2({theme:'bootstrap4'});
    });
</script>
@endsection