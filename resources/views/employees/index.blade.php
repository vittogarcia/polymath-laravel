@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('page_title')
Employees
@endsection

@section('content')
<div class="card card-primary card-outline">
	<div class="card-body">
		<div class="d-flex justify-content-end">
			<a class="btn btn-app" href="{{ url('employees/create') }}">
              <i class="fas fa-plus"></i> New Employee
            </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-hover table-sm" id="employees_table">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Company</th>
					<th>Show</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $e)
				<tr>
					<td>{{ $e->first_name }}</td>
					<td>{{ $e->last_name }}</td>
					<td>{{ $e->email }}</td>
					<td>{{ $e->phone }}</td>
					<td>{{ $e->companies->name }}</td>
					<td>
						<a href="/employees/{{ $e->id }}" class="btn"><i class="far fa-eye"></i></a>
					</td>
					<td>
						<a href="/employees/{{ $e->id }}/edit" class="btn"><i class="far fa-edit"></i></a>
					</td>
					<td>
						<form action="/employees/{{ $e->id }}" method="POST">@csrf @method('DELETE')
							<button type="submit" class="btn"><i class="far fa-trash-alt"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>	
</div>
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#employees_table').DataTable({
            "paging": true,
            "ordering": true,
            "pageLength": 10,
            "lengthMenu": [10, 20, 30, 40, 50]
        });
    });       
</script>
@endsection