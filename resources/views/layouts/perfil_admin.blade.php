<li class="nav-item dropdown">
  <a class="nav-link" data-toggle="dropdown" href="#">
    <span style="margin-right: 5px;">{{ Auth::user()->name }}</span><i class="far fa-user-circle"></i> 
  </a>
  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#logoutModal">
      <i class="fas fa-power-off mr-2"></i> Log Out
    </a>
  </div>
</li>