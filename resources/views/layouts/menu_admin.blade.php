<div class="collapse navbar-collapse order-3" id="navbarCollapse">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="{{ url('companies') }}" class="nav-link">Companies</a>
      </li>        
      <li class="nav-item">
        <a href="{{ url('employees') }}" class="nav-link">Employees</a>
      </li> 
    </ul>
  </div>